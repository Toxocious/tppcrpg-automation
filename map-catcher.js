// ==UserScript==
// @name         TPPC v8: Map Script
// @description  Casually taking over the world.
// @match        https://www.tppcrpg.net/map.php*
// @require      https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js
// @namespace    https://gitlab.com/toxocious
// @author       Toxocious
// @version      8.0
// ==/UserScript==

// Change the map ID to w/e you want.
var map_id = '1';

$(function()
{
  setInterval(function()
  {
    // #inner is missing.
    if ( $('#inner').length < 1 )
    {
      window.location = 'http://www.tppcrpg.net/map.php?Map=' + map_id;
    }

    // Encountered an NPC battle; returning you to the maps.
    else if ( $('#inner > h2').text().indexOf("You Don't Belong Here") > -1 )
    {
      console.log("encountered an npc battle");
      window.location = 'http://www.tppcrpg.net/map.php?Map=' + map_id;
    }

    // Encountered a Pokemon; catching it.
    else if ( $('#inner form blockquote').text().indexOf('You found a(n)') > -1 )
    {
      console.log("found a pokemon");
      $('#inner form blockquote input[value="Catch It!"]').click();
    }

    // If you caught a Pokemon, return to the map.
    else if ( $('#inner > h2').text().indexOf("Congratulations") > -1 )
    {
      console.log("caught a pokemon; returning to the map");
      window.location = 'http://www.tppcrpg.net/map.php?Map=' + map_id;
    }

    // If you didn't find a pokemon or encounter an NPC battle; search the map.
    else
    {
      $('#inner > form > p > input[type="image"]').click();
    }
  }, Math.random() * 250 + 400);
});
