# TPPCRPG Automation
Various JavaScript scripts that enhance and automate features on TPPCRPG.

**NOTE**: I will most likely not maintain these scripts, which means they will likely not be updated.
Last I knew, these all worked, and the game itself has not seen a major update in near a decade, so these should all still work.

# How To Use
These scripts were created to be used in your favorite brower-based JavaScript userscript manager, such as Greasemonkey or Tampermonkey (tested in Tampermonkey, assumed to work in Greasemonkey).

Copy the contents of the given script that you would like to make use of, and create a new userscript within Greasemonkey or Tampermonkey.
Save the script within the userscript manager, and the next time you visit the correct page(s) that the script uses, the automation will begin.

## Battle Script (battle-script.js)
This script, as the name suggests, will auto-battle the trainer that is specified within the file.

The script includes **29** hand-selected Chansey/Blissey gyms to choose from, ranging from very low to very high levels.

**Last Updated**: 13/02/2021

## Donation Center (donation-center.js)
This script will automaticall donate Pok&eacute;mon to the Donation Center.

Pokemon that meet any of the following specifications are skipped, and not donated:
- Level 5
- (?) Gendered
- Shiny
- Dark
- Golden

**Last Updated**: 30/01/2018

## Map Catcher (map-catcher.js)
This script will automatically search for, and catch, Pok&eacute;mon that are encountered on the maps.

A script ID (taken from the desired map URL) must be specified; default is map ID #1.

**Last Updated**: 30/01/2018

## S.S. Anne Script (ss-anne.js)
This script will auto-battle the S.S. Anne battle challenge, and attempt to get a high streak or high score.

A team of Chansey and/or Blissey is required for this script to work.

The team of Chansey and/or Blissey need to know the moves **Ice Beam**, **Ancient Power**, **Double Edge**, and **Giga Drain**, or the script will not start.

**Last Updated**: 21//01/2018

## Training Challenge Script (training-challenge.js)
This script will auto-battle Pok&eacute;mon within the Training Challenge.

This works with any Pok&eacute;mon that is permitted to participate within the Training Challenge.

The Pok&eacute;mon must have the moves **Close Combat**, **Earthquake**, and **Ice Ball**, or the script will not work.

The script will check the opposing Pok&eacute;mon's typings, and attempt to use the best possible move out of its moveset.

**Last Updated**: 14/01/2018
