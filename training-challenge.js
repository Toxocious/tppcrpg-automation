// ==UserScript==
// @name         TPPC v8: Training Challenge
// @description  Auto-battles during the Training Challenge.
// @include      https://*tppcrpg.net*
// @grant        GM_deleteValue
// @grant        GM_getValue
// @grant        GM_setValue
// @namespace    https://gitlab.com/toxocious
// @author       Toxocious
// @version      8.0
// ==/UserScript==

// VARIABLES :: Change this.
goalLevel = '1000';

// VARIABLES :: Don't change these.
randClick = 0;
clicked = 0;
count = 0;
set = 0;
inputWidth = Math.floor(Math.random() * 50 + 20);
inputHeight = Math.floor(Math.random() * 6 + 10);

// Sleep function.
function sleep(miliseconds)
{
  var currentTime = new Date().getTime();
  while (currentTime + miliseconds >= new Date().getTime()) {}
}

// Execute the script whenever the DOM has loaded.
$(function()
{
  if ( window.location.href.indexOf("TrainingChallenge") > -1 )
  {
    interval = setInterval(function()
    {
      // Alert once you've reached your set Goal Level.
      if (document.body.innerHTML.search("has reached level (" + goalLevel + ")") != -1)
      {
        clearInterval(interval);
        alert("Done!");
        return true;
      }

      // Track random battle pauses.
      $(document).ready(function()
      {
        if (!GM_getValue("shortpause"))
          GM_setValue("shortpause", 0);

        if (!GM_getValue("longpause"))
          GM_setValue("longpause", 0);

        if (set === 0)
          $(".lvlitem").append("<br><a href='#' style='color:#FFFFFF !important;text-decoration:none;'>Pause Count:</a> " + GM_getValue("shortpause") + " <br/>Long Pause: " + GM_getValue("longpause"));

        set = 1;

        $(".lvlitem a").click(function()
        {
          if (clicked === 0)
          {
            if (confirm("Reset counter?"))
            {
              clicked = 1;
              clearInterval(interval);
              GM_deleteValue("longpause");
              GM_deleteValue("shortpause");
            }
          }
        });
      });

      // Restart battle with a random delay.
      links = document.getElementsByTagName("a");
      if ($("#cancelBattle > A").html() == "Restart Battle" && count === 0)
      {
        count++;
        myDelay = 200;
        randInt = Math.floor(Math.random() * 200 + 1);

        if (randInt == 1)
        {
          randDelay = Math.floor(Math.random() * 300 + Math.random() * 200 + 300);
          myDelay = myDelay * randDelay;
          var longpause = GM_getValue("longpause") + 1;
          GM_setValue("longpause", longpause);
        }

        setTimeout(function()
        {
          var restart = document.location.href;
					restart = restart.replace(/battle\.php.*$/, '');
					restart += 'battle.php?Battle=TrainingChallenge&Trainer=-1';

					document.location = restart;
        }, Math.random() * 400 + myDelay);
      }

      // Get battle button position and top left, bottom right coords.
      x = $('.submit').position();
      cordX = x.left;
      cordY = x.top;

      if (document.body.innerHTML.lastIndexOf(/Loading.../) == -1)
      {
        inputs = document.getElementsByTagName("input");
        randInt = Math.floor(Math.random() * 720 + 1);

        if (randInt == 1)
        {
          randClick = Math.floor(Math.random() * 25000) + 5000;
          sleep(randClick);

          var shortpause = GM_getValue("shortpause") + 1;
          GM_setValue("shortpause", shortpause);
        }

        // Attack the foe.
        for ( i = 0; i < inputs.length; i++ )
        {
          if ( inputs[i].value.search(/(Attack)/) != -1 )
          {
            // Verify that the user has the correct moves.
            // Move #1: Close Combat
            // Move #2: Earthquake
            // Move #3: Ice Ball
            if
            (
              $('#MyMove option:nth-child(1)').val() !== '48' &&
              $('#MyMove option:nth-child(2)').val() !== '138' &&
              $('#MyMove option:nth-child(3)').val() !== '148'
            )
            {
              clearInterval(interval);
              alert('Please change your moves to: Close Combat, Earthquake, and Ice Ball.');
              return true;
            }

            count = 0;

            // Grab the foe's current Pokemon.
            var foe = $('#Trainer2_Active > div.headerBar').text().split("TPPC Battle Master's ", 2).join("");

            // Iterate through the 'pokemon' array until it finds the foe's active Pokemon.
            $.each(pokemon, function(poke, types)
            {
              if ( poke === foe )
              {
                // If the foe is weak to Ice-type moves.
                if ( types.type1 == 'Grass' || types.type1 == 'Ground' || types.type1 == 'Flying' || types.type1 == 'Dragon' )
                {
                  Move = '148';
                }
                // If the foe is weak to Ground-type moves.
                else if ( types.type1 == 'Fire' || types.type1 == 'Electric' || types.type1 == 'Poison' )
                {
                  Move = '138';
                }
                // Else default to a Fighting-type move for most coverage.
                else
                {
                  Move = '48';
                }

                return false;
              }
            });

            unsafeWindow.getBattleStatus({
              "MyMove": Move,
              "pageID": Math.floor(Math.random() * inputHeight + cordX + inputWidth) + "." + Math.floor(Math.random() * 18 + cordY + 3)
            });
          }
        }
      }
    }, Math.random() * 200 + 400);
  }
});

/* =========== Captcha Stuff =========== */
$(document).ready(function()
{
  // If a captcha shows on screen, alert the user
	if (document.body.innerHTML.search(/Please Enter The Combination You See Above/) != -1)
  {
    console.log('Captcha!');
		alert("Captcha!");
	}

  // Once the user has submitted the correct captcha, go to the training challenge page.
	if (document.body.innerHTML.search(/You can now resume battling/) != -1)
  {
    console.log('Redirecting the script to the Training Challenge page.');
		window.location = "http://www.tppcrpg.net/training_challenge.php";
	}

  // Once on the training challenge page, begin battling.
	if (window.location == "http://www.tppcrpg.net/training_challenge.php")
  {
    console.log('Beginning the battle against the Training Challenge script.');
		window.location = "http://www.tppcrpg.net/battle.php?Battle=TrainingChallenge";
	}
});

/* =========== Pokemon Array =========== */
var pokemon = {
	"Bulbasaur": { type1: "Grass", type2: "Poison", },
	"Ivysaur": { type1: "Grass", type2: "Poison", },
	"Venusaur": { type1: "Grass", type2: "Poison", },
	"Charmander": { type1: "Fire", type2: "None", },
	"Charmeleon": { type1: "Fire", type2: "None", },
	"Charizard": { type1: "Fire", type2: "Flying", },
	"Squirtle": { type1: "Water", type2: "None", },
	"Wartortle": { type1: "Water", type2: "None", },
	"Blastoise": { type1: "Water", type2: "None", },
	"Caterpie": { type1: "Bug", type2: "None", },
	"Metapod": { type1: "Bug", type2: "None", },
	"Butterfree": { type1: "Bug", type2: "Flying", },
	"Weedle": { type1: "Bug", type2: "Poison", },
	"Kakuna": { type1: "Bug", type2: "Poison", },
	"Beedrill": { type1: "Bug", type2: "Poison", },
	"Pidgey": { type1: "Normal", type2: "Flying", },
	"Pidgeotto": { type1: "Normal", type2: "Flying", },
	"Pidgeot": { type1: "Normal", type2: "Flying", },
	"Rattata": { type1: "Normal", type2: "None", },
	"Raticate": { type1: "Normal", type2: "None", },
	"Spearow": { type1: "Normal", type2: "Flying", },
	"Fearow": { type1: "Normal", type2: "Flying", },
	"Ekans": { type1: "Poison", type2: "None", },
	"Arbok": { type1: "Poison", type2: "None", },
	"Pikachu": { type1: "Electric", type2: "None", },
	"Raichu": { type1: "Electric", type2: "None", },
	"Sandshrew": { type1: "Ground", type2: "None", },
	"Sandslash": { type1: "Ground", type2: "None", },
	"Nidoran(F)": { type1: "Poison", type2: "None", },
	"Nidorina": { type1: "Poison", type2: "None", },
	"Nidoqueen": { type1: "Poison", type2: "Ground", },
	"Nidoran(M)": { type1: "Poison", type2: "None", },
	"Nidorino": { type1: "Poison", type2: "None", },
	"Nidoking": { type1: "Poison", type2: "Ground", },
	"Clefairy": { type1: "Fairy", type2: "None", },
	"Clefable": { type1: "Fairy", type2: "None", },
	"Vulpix": { type1: "Fire", type2: "None", },
	"Ninetales": { type1: "Fire", type2: "None", },
	"Jigglypuff": { type1: "Normal", type2: "Fairy", },
	"Wigglytuff": { type1: "Normal", type2: "Fairy", },
	"Zubat": { type1: "Poison", type2: "Flying", },
	"Golbat": { type1: "Poison", type2: "Flying", },
	"Oddish": { type1: "Grass", type2: "Poison", },
	"Gloom": { type1: "Grass", type2: "Poison", },
	"Vileplume": { type1: "Grass", type2: "Poison", },
	"Paras": { type1: "Bug", type2: "Grass", },
	"Parasect": { type1: "Bug", type2: "Grass", },
	"Venonat": { type1: "Bug", type2: "Poison", },
	"Venomoth": { type1: "Bug", type2: "Poison", },
	"Diglett": { type1: "Ground", type2: "None", },
	"Dugtrio": { type1: "Ground", type2: "None", },
	"Meowth": { type1: "Normal", type2: "None", },
	"Persian": { type1: "Normal", type2: "None", },
	"Psyduck": { type1: "Water", type2: "None", },
	"Golduck": { type1: "Water", type2: "None", },
	"Mankey": { type1: "Fighting", type2: "None", },
	"Primeape": { type1: "Fighting", type2: "None", },
	"Growlithe": { type1: "Fire", type2: "None", },
	"Arcanine": { type1: "Fire", type2: "None", },
	"Poliwag": { type1: "Water", type2: "None", },
	"Poliwhirl": { type1: "Water", type2: "None", },
	"Poliwrath": { type1: "Water", type2: "Fighting", },
	"Abra": { type1: "Psychic", type2: "None", },
	"Kadabra": { type1: "Psychic", type2: "None", },
	"Alakazam": { type1: "Psychic", type2: "None", },
	"Machop": { type1: "Fighting", type2: "None", },
	"Machoke": { type1: "Fighting", type2: "None", },
	"Machamp": { type1: "Fighting", type2: "None", },
	"Bellsprout": { type1: "Grass", type2: "Poison", },
	"Weepinbell": { type1: "Grass", type2: "Poison", },
	"Victreebel": { type1: "Grass", type2: "Poison", },
	"Tentacool": { type1: "Water", type2: "Poison", },
	"Tentacruel": { type1: "Water", type2: "Poison", },
	"Geodude": { type1: "Rock", type2: "Ground", },
	"Graveler": { type1: "Rock", type2: "Ground", },
	"Golem": { type1: "Rock", type2: "Ground", },
	"Ponyta": { type1: "Fire", type2: "None", },
	"Rapidash": { type1: "Fire", type2: "None", },
	"Slowpoke": { type1: "Water", type2: "Psychic", },
	"Slowbro": { type1: "Water", type2: "Psychic", },
	"Magnemite": { type1: "Electric", type2: "Steel", },
	"Magneton": { type1: "Electric", type2: "Steel", },
	"Farfetch'd": { type1: "Normal", type2: "Flying", },
	"Doduo": { type1: "Normal", type2: "Flying", },
	"Dodrio": { type1: "Normal", type2: "Flying", },
	"Seel": { type1: "Water", type2: "None", },
	"Dewgong": { type1: "Water", type2: "Ice", },
	"Grimer": { type1: "Poison", type2: "None", },
	"Muk": { type1: "Poison", type2: "None", },
	"Shellder": { type1: "Water", type2: "None", },
	"Cloyster": { type1: "Water", type2: "Ice", },
	"Gastly": { type1: "Ghost", type2: "Poison", },
	"Haunter": { type1: "Ghost", type2: "Poison", },
	"Gengar": { type1: "Ghost", type2: "Poison", },
	"Onix": { type1: "Rock", type2: "Ground", },
	"Drowzee": { type1: "Psychic", type2: "None", },
	"Hypno": { type1: "Psychic", type2: "None", },
	"Krabby": { type1: "Water", type2: "None", },
	"Kingler": { type1: "Water", type2: "None", },
	"Voltorb": { type1: "Electric", type2: "None", },
	"Electrode": { type1: "Electric", type2: "None", },
	"Exeggcute": { type1: "Grass", type2: "Psychic", },
	"Exeggutor": { type1: "Grass", type2: "Psychic", },
	"Cubone": { type1: "Ground", type2: "None", },
	"Marowak": { type1: "Ground", type2: "None", },
	"Hitmonlee": { type1: "Fighting", type2: "None", },
	"Hitmonchan": { type1: "Fighting", type2: "None", },
	"Lickitung": { type1: "Normal", type2: "None", },
	"Koffing": { type1: "Poison", type2: "None", },
	"Weezing": { type1: "Poison", type2: "None", },
	"Rhyhorn": { type1: "Ground", type2: "Rock", },
	"Rhydon": { type1: "Ground", type2: "Rock", },
	"Chansey": { type1: "Normal", type2: "None", },
	"Tangela": { type1: "Grass", type2: "None", },
	"Kangaskhan": { type1: "Normal", type2: "None", },
	"Horsea": { type1: "Water", type2: "None", },
	"Seadra": { type1: "Water", type2: "None", },
	"Goldeen": { type1: "Water", type2: "None", },
	"Seaking": { type1: "Water", type2: "None", },
	"Staryu": { type1: "Water", type2: "None", },
	"Starmie": { type1: "Water", type2: "Psychic", },
	"Mr. Mime": { type1: "Psychic", type2: "Fairy", },
	"Scyther": { type1: "Bug", type2: "Flying", },
	"Jynx": { type1: "Ice", type2: "Psychic", },
	"Electabuzz": { type1: "Electric", type2: "None", },
	"Magmar": { type1: "Fire", type2: "None", },
	"Pinsir": { type1: "Bug", type2: "None", },
	"Tauros": { type1: "Normal", type2: "None", },
	"Magikarp": { type1: "Water", type2: "None", },
	"Gyarados": { type1: "Water", type2: "Flying", },
	"Lapras": { type1: "Water", type2: "Ice", },
	"Ditto": { type1: "Normal", type2: "None", },
	"Eevee": { type1: "Normal", type2: "None", },
	"Vaporeon": { type1: "Water", type2: "None", },
	"Jolteon": { type1: "Electric", type2: "None", },
	"Flareon": { type1: "Fire", type2: "None", },
	"Porygon": { type1: "Normal", type2: "None", },
	"Omanyte": { type1: "Rock", type2: "Water", },
	"Omastar": { type1: "Rock", type2: "Water", },
	"Kabuto": { type1: "Rock", type2: "Water", },
	"Kabutops": { type1: "Rock", type2: "Water", },
	"Aerodactyl": { type1: "Rock", type2: "Flying", },
	"Snorlax": { type1: "Normal", type2: "None", },
	"Articuno": { type1: "Ice", type2: "Flying", },
	"Zapdos": { type1: "Electric", type2: "Flying", },
	"Moltres": { type1: "Fire", type2: "Flying", },
	"Dratini": { type1: "Dragon", type2: "None", },
	"Dragonair": { type1: "Dragon", type2: "None", },
	"Dragonite": { type1: "Dragon", type2: "Flying", },
	"Mewtwo": { type1: "Psychic", type2: "None", },
	"Mew": { type1: "Psychic", type2: "None", },
	"Chikorita": { type1: "Grass", type2: "None", },
	"Bayleef": { type1: "Grass", type2: "None", },
	"Meganium": { type1: "Grass", type2: "None", },
	"Cyndaquil": { type1: "Fire", type2: "None", },
	"Quilava": { type1: "Fire", type2: "None", },
	"Typhlosion": { type1: "Fire", type2: "None", },
	"Totodile": { type1: "Water", type2: "None", },
	"Croconaw": { type1: "Water", type2: "None", },
	"Feraligatr": { type1: "Water", type2: "None", },
	"Sentret": { type1: "Normal", type2: "None", },
	"Furret": { type1: "Normal", type2: "None", },
	"Hoothoot": { type1: "Normal", type2: "Flying", },
	"Noctowl": { type1: "Normal", type2: "Flying", },
	"Ledyba": { type1: "Bug", type2: "Flying", },
	"Ledian": { type1: "Bug", type2: "Flying", },
	"Spinarak": { type1: "Bug", type2: "Poison", },
	"Ariados": { type1: "Bug", type2: "Poison", },
	"Crobat": { type1: "Poison", type2: "Flying", },
	"Chinchou": { type1: "Water", type2: "Electric", },
	"Lanturn": { type1: "Water", type2: "Electric", },
	"Pichu": { type1: "Electric", type2: "None", },
	"Cleffa": { type1: "Fairy", type2: "None", },
	"Igglybuff": { type1: "Normal", type2: "Fairy", },
	"Togepi": { type1: "Fairy", type2: "None", },
	"Togetic": { type1: "Fairy", type2: "Flying", },
	"Natu": { type1: "Psychic", type2: "Flying", },
	"Xatu": { type1: "Psychic", type2: "Flying", },
	"Mareep": { type1: "Electric", type2: "None", },
	"Flaaffy": { type1: "Electric", type2: "None", },
	"Ampharos": { type1: "Electric", type2: "None", },
	"Bellossom": { type1: "Grass", type2: "None", },
	"Marill": { type1: "Water", type2: "Fairy", },
	"Azumarill": { type1: "Water", type2: "Fairy", },
	"Sudowoodo": { type1: "Rock", type2: "None", },
	"Politoed": { type1: "Water", type2: "None", },
	"Hoppip": { type1: "Grass", type2: "Flying", },
	"Skiploom": { type1: "Grass", type2: "Flying", },
	"Jumpluff": { type1: "Grass", type2: "Flying", },
	"Aipom": { type1: "Normal", type2: "None", },
	"Sunkern": { type1: "Grass", type2: "None", },
	"Sunflora": { type1: "Grass", type2: "None", },
	"Yanma": { type1: "Bug", type2: "Flying", },
	"Wooper": { type1: "Water", type2: "Ground", },
	"Quagsire": { type1: "Water", type2: "Ground", },
	"Espeon": { type1: "Psychic", type2: "None", },
	"Umbreon": { type1: "Dark", type2: "None", },
	"Murkrow": { type1: "Dark", type2: "Flying", },
	"Slowking": { type1: "Water", type2: "Psychic", },
	"Misdreavus": { type1: "Ghost", type2: "None", },
	"UnownA": { type1: "Psychic", type2: "None", },
	"Wobbuffet": { type1: "Psychic", type2: "None", },
	"Girafarig": { type1: "Normal", type2: "Psychic", },
	"Pineco": { type1: "Bug", type2: "None", },
	"Forretress": { type1: "Bug", type2: "Steel", },
	"Dunsparce": { type1: "Normal", type2: "None", },
	"Gligar": { type1: "Ground", type2: "Flying", },
	"Steelix": { type1: "Steel", type2: "Ground", },
	"Snubbull": { type1: "Fairy", type2: "None", },
	"Granbull": { type1: "Fairy", type2: "None", },
	"Qwilfish": { type1: "Water", type2: "Poison", },
	"Scizor": { type1: "Bug", type2: "Steel", },
	"Shuckle": { type1: "Bug", type2: "Rock", },
	"Heracross": { type1: "Bug", type2: "Fighting", },
	"Sneasel": { type1: "Dark", type2: "Ice", },
	"Teddiursa": { type1: "Normal", type2: "None", },
	"Ursaring": { type1: "Normal", type2: "None", },
	"Slugma": { type1: "Fire", type2: "None", },
	"Magcargo": { type1: "Fire", type2: "Rock", },
	"Swinub": { type1: "Ice", type2: "Ground", },
	"Piloswine": { type1: "Ice", type2: "Ground", },
	"Corsola": { type1: "Water", type2: "Rock", },
	"Remoraid": { type1: "Water", type2: "None", },
	"Octillery": { type1: "Water", type2: "None", },
	"Delibird": { type1: "Ice", type2: "Flying", },
	"Mantine": { type1: "Water", type2: "Flying", },
	"Skarmory": { type1: "Steel", type2: "Flying", },
	"Houndour": { type1: "Dark", type2: "Fire", },
	"Houndoom": { type1: "Dark", type2: "Fire", },
	"Kingdra": { type1: "Water", type2: "Dragon", },
	"Phanpy": { type1: "Ground", type2: "None", },
	"Donphan": { type1: "Ground", type2: "None", },
	"Porygon 2": { type1: "Normal", type2: "None", },
	"Stantler": { type1: "Normal", type2: "None", },
	"Smeargle": { type1: "Normal", type2: "None", },
	"Tyrogue": { type1: "Fighting", type2: "None", },
	"Hitmontop": { type1: "Fighting", type2: "None", },
	"Smoochum": { type1: "Ice", type2: "Psychic", },
	"Elekid": { type1: "Electric", type2: "None", },
	"Magby": { type1: "Fire", type2: "None", },
	"Miltank": { type1: "Normal", type2: "None", },
	"Blissey": { type1: "Normal", type2: "None", },
	"Raikou": { type1: "Electric", type2: "None", },
	"Entei": { type1: "Fire", type2: "None", },
	"Suicune": { type1: "Water", type2: "None", },
	"Larvitar": { type1: "Rock", type2: "Ground", },
	"Pupitar": { type1: "Rock", type2: "Ground", },
	"Tyranitar": { type1: "Rock", type2: "Dark", },
	"Lugia": { type1: "Psychic", type2: "Flying", },
	"Ho-Oh": { type1: "Fire", type2: "Flying", },
	"Celebi": { type1: "Psychic", type2: "Grass", },
	"Treecko": { type1: "Grass", type2: "None", },
	"Grovyle": { type1: "Grass", type2: "None", },
	"Sceptile": { type1: "Grass", type2: "None", },
	"Torchic": { type1: "Fire", type2: "None", },
	"Combusken": { type1: "Fire", type2: "Fighting", },
	"Blaziken": { type1: "Fire", type2: "Fighting", },
	"Mudkip": { type1: "Water", type2: "None", },
	"Marshtomp": { type1: "Water", type2: "Ground", },
	"Swampert": { type1: "Water", type2: "Ground", },
	"Poochyena": { type1: "Dark", type2: "None", },
	"Mightyena": { type1: "Dark", type2: "None", },
	"Zigzagoon": { type1: "Normal", type2: "None", },
	"Linoone": { type1: "Normal", type2: "None", },
	"Wurmple": { type1: "Bug", type2: "None", },
	"Silcoon": { type1: "Bug", type2: "None", },
	"Beautifly": { type1: "Bug", type2: "Flying", },
	"Cascoon": { type1: "Bug", type2: "None", },
	"Dustox": { type1: "Bug", type2: "Poison", },
	"Lotad": { type1: "Water", type2: "Grass", },
	"Lombre": { type1: "Water", type2: "Grass", },
	"Ludicolo": { type1: "Water", type2: "Grass", },
	"Seedot": { type1: "Grass", type2: "None", },
	"Nuzleaf": { type1: "Grass", type2: "Dark", },
	"Shiftry": { type1: "Grass", type2: "Dark", },
	"Taillow": { type1: "Normal", type2: "Flying", },
	"Swellow": { type1: "Normal", type2: "Flying", },
	"Wingull": { type1: "Water", type2: "Flying", },
	"Pelipper": { type1: "Water", type2: "Flying", },
	"Ralts": { type1: "Psychic", type2: "Fairy", },
	"Kirlia": { type1: "Psychic", type2: "Fairy", },
	"Gardevoir": { type1: "Psychic", type2: "Fairy", },
	"Surskit": { type1: "Bug", type2: "Water", },
	"Masquerain": { type1: "Bug", type2: "Flying", },
	"Shroomish": { type1: "Grass", type2: "None", },
	"Breloom": { type1: "Grass", type2: "Fighting", },
	"Slakoth": { type1: "Normal", type2: "None", },
	"Vigoroth": { type1: "Normal", type2: "None", },
	"Slaking": { type1: "Normal", type2: "None", },
	"Nincada": { type1: "Bug", type2: "Ground", },
	"Ninjask": { type1: "Bug", type2: "Flying", },
	"Shedinja": { type1: "Bug", type2: "Ghost", },
	"Whismur": { type1: "Normal", type2: "None", },
	"Loudred": { type1: "Normal", type2: "None", },
	"Exploud": { type1: "Normal", type2: "None", },
	"Makuhita": { type1: "Fighting", type2: "None", },
	"Hariyama": { type1: "Fighting", type2: "None", },
	"Azurill": { type1: "Normal", type2: "Fairy", },
	"Nosepass": { type1: "Rock", type2: "None", },
	"Skitty": { type1: "Normal", type2: "None", },
	"Delcatty": { type1: "Normal", type2: "None", },
	"Sableye": { type1: "Dark", type2: "Ghost", },
	"Mawile": { type1: "Steel", type2: "Fairy", },
	"Aron": { type1: "Steel", type2: "Rock", },
	"Lairon": { type1: "Steel", type2: "Rock", },
	"Aggron": { type1: "Steel", type2: "Rock", },
	"Meditite": { type1: "Fighting", type2: "Psychic", },
	"Medicham": { type1: "Fighting", type2: "Psychic", },
	"Electrike": { type1: "Electric", type2: "None", },
	"Manectric": { type1: "Electric", type2: "None", },
	"Plusle": { type1: "Electric", type2: "None", },
	"Minun": { type1: "Electric", type2: "None", },
	"Volbeat": { type1: "Bug", type2: "None", },
	"Illumise": { type1: "Bug", type2: "None", },
	"Roselia": { type1: "Grass", type2: "Poison", },
	"Gulpin": { type1: "Poison", type2: "None", },
	"Swalot": { type1: "Poison", type2: "None", },
	"Carvanha": { type1: "Water", type2: "Dark", },
	"Sharpedo": { type1: "Water", type2: "Dark", },
	"Wailmer": { type1: "Water", type2: "None", },
	"Wailord": { type1: "Water", type2: "None", },
	"Numel": { type1: "Fire", type2: "Ground", },
	"Camerupt": { type1: "Fire", type2: "Ground", },
	"Torkoal": { type1: "Fire", type2: "None", },
	"Spoink": { type1: "Psychic", type2: "None", },
	"Grumpig": { type1: "Psychic", type2: "None", },
	"Spinda": { type1: "Normal", type2: "None", },
	"Trapinch": { type1: "Ground", type2: "None", },
	"Vibrava": { type1: "Ground", type2: "Dragon", },
	"Flygon": { type1: "Ground", type2: "Dragon", },
	"Cacnea": { type1: "Grass", type2: "None", },
	"Cacturne": { type1: "Grass", type2: "Dark", },
	"Swablu": { type1: "Normal", type2: "Flying", },
	"Altaria": { type1: "Dragon", type2: "Flying", },
	"Zangoose": { type1: "Normal", type2: "None", },
	"Seviper": { type1: "Poison", type2: "None", },
	"Lunatone": { type1: "Rock", type2: "Psychic", },
	"Solrock": { type1: "Rock", type2: "Psychic", },
	"Barboach": { type1: "Water", type2: "Ground", },
	"Whiscash": { type1: "Water", type2: "Ground", },
	"Corphish": { type1: "Water", type2: "None", },
	"Crawdaunt": { type1: "Water", type2: "Dark", },
	"Baltoy": { type1: "Ground", type2: "Psychic", },
	"Claydol": { type1: "Ground", type2: "Psychic", },
	"Lileep": { type1: "Rock", type2: "Grass", },
	"Cradily": { type1: "Rock", type2: "Grass", },
	"Anorith": { type1: "Rock", type2: "Bug", },
	"Armaldo": { type1: "Rock", type2: "Bug", },
	"Feebas": { type1: "Water", type2: "None", },
	"Milotic": { type1: "Water", type2: "None", },
	"Castform": { type1: "Normal", type2: "None", },
	"Kecleon": { type1: "Normal", type2: "None", },
	"Shuppet": { type1: "Ghost", type2: "None", },
	"Banette": { type1: "Ghost", type2: "None", },
	"Duskull": { type1: "Ghost", type2: "None", },
	"Dusclops": { type1: "Ghost", type2: "None", },
	"Tropius": { type1: "Grass", type2: "Flying", },
	"Chimecho": { type1: "Psychic", type2: "None", },
	"Absol": { type1: "Dark", type2: "None", },
	"Wynaut": { type1: "Psychic", type2: "None", },
	"Snorunt": { type1: "Ice", type2: "None", },
	"Glalie": { type1: "Ice", type2: "None", },
	"Spheal": { type1: "Ice", type2: "Water", },
	"Sealeo": { type1: "Ice", type2: "Water", },
	"Walrein": { type1: "Ice", type2: "Water", },
	"Clamperl": { type1: "Water", type2: "None", },
	"Huntail": { type1: "Water", type2: "None", },
	"Gorebyss": { type1: "Water", type2: "None", },
	"Relicanth": { type1: "Water", type2: "Rock", },
	"Luvdisc": { type1: "Water", type2: "None", },
	"Bagon": { type1: "Dragon", type2: "None", },
	"Shelgon": { type1: "Dragon", type2: "None", },
	"Salamence": { type1: "Dragon", type2: "Flying", },
	"Beldum": { type1: "Steel", type2: "Psychic", },
	"Metang": { type1: "Steel", type2: "Psychic", },
	"Metagross": { type1: "Steel", type2: "Psychic", },
	"Regirock": { type1: "Rock", type2: "None", },
	"Regice": { type1: "Ice", type2: "None", },
	"Registeel": { type1: "Steel", type2: "None", },
	"Latias": { type1: "Dragon", type2: "Psychic", },
	"Latios": { type1: "Dragon", type2: "Psychic", },
	"Kyogre": { type1: "Water", type2: "None", },
	"Groudon": { type1: "Ground", type2: "None", },
	"Rayquaza": { type1: "Dragon", type2: "Flying", },
	"Jirachi": { type1: "Steel", type2: "Psychic", },
	"Deoxys": { type1: "Psychic", type2: "None", },
	"Turtwig": { type1: "Grass", type2: "None", },
	"Grotle": { type1: "Grass", type2: "None", },
	"Torterra": { type1: "Grass", type2: "Ground", },
	"Chimchar": { type1: "Fire", type2: "None", },
	"Monferno": { type1: "Fire", type2: "Fighting", },
	"Infernape": { type1: "Fire", type2: "Fighting", },
	"Piplup": { type1: "Water", type2: "None", },
	"Prinplup": { type1: "Water", type2: "None", },
	"Empoleon": { type1: "Water", type2: "Steel", },
	"Starly": { type1: "Normal", type2: "Flying", },
	"Staravia": { type1: "Normal", type2: "Flying", },
	"Staraptor": { type1: "Normal", type2: "Flying", },
	"Bidoof": { type1: "Normal", type2: "None", },
	"Bibarel": { type1: "Normal", type2: "Water", },
	"Kricketot": { type1: "Bug", type2: "None", },
	"Kricketune": { type1: "Bug", type2: "None", },
	"Shinx": { type1: "Electric", type2: "None", },
	"Luxio": { type1: "Electric", type2: "None", },
	"Luxray": { type1: "Electric", type2: "None", },
	"Budew": { type1: "Grass", type2: "Poison", },
	"Roserade": { type1: "Grass", type2: "Poison", },
	"Cranidos": { type1: "Rock", type2: "None", },
	"Rampardos": { type1: "Rock", type2: "None", },
	"Shieldon": { type1: "Rock", type2: "Steel", },
	"Bastiodon": { type1: "Rock", type2: "Steel", },
	"Burmy": { type1: "Bug", type2: "None", },
	"Wormadam (Plant Cloak)": { type1: "Bug", type2: "Grass", },
	"Mothim": { type1: "Bug", type2: "Flying", },
	"Combee": { type1: "Bug", type2: "Flying", },
	"Vespiquen": { type1: "Bug", type2: "Flying", },
	"Pachirisu": { type1: "Electric", type2: "None", },
	"Buizel": { type1: "Water", type2: "None", },
	"Floatzel": { type1: "Water", type2: "None", },
	"Cherubi": { type1: "Grass", type2: "None", },
	"Cherrim": { type1: "Grass", type2: "None", },
	"Shellos": { type1: "Water", type2: "None", },
	"Gastrodon": { type1: "Water", type2: "Ground", },
	"Ambipom": { type1: "Normal", type2: "None", },
	"Drifloon": { type1: "Ghost", type2: "Flying", },
	"Drifblim": { type1: "Ghost", type2: "Flying", },
	"Buneary": { type1: "Normal", type2: "None", },
	"Lopunny": { type1: "Normal", type2: "None", },
	"Mismagius": { type1: "Ghost", type2: "None", },
	"Honchkrow": { type1: "Dark", type2: "Flying", },
	"Glameow": { type1: "Normal", type2: "None", },
	"Purugly": { type1: "Normal", type2: "None", },
	"Chingling": { type1: "Psychic", type2: "None", },
	"Stunky": { type1: "Poison", type2: "Dark", },
	"Skuntank": { type1: "Poison", type2: "Dark", },
	"Bronzor": { type1: "Steel", type2: "Psychic", },
	"Bronzong": { type1: "Steel", type2: "Psychic", },
	"Bonsly": { type1: "Rock", type2: "None", },
	"Mime Jr.": { type1: "Psychic", type2: "Fairy", },
	"Happiny": { type1: "Normal", type2: "None", },
	"Chatot": { type1: "Normal", type2: "Flying", },
	"Spiritomb": { type1: "Ghost", type2: "Dark", },
	"Gible": { type1: "Dragon", type2: "Ground", },
	"Gabite": { type1: "Dragon", type2: "Ground", },
	"Garchomp": { type1: "Dragon", type2: "Ground", },
	"Munchlax": { type1: "Normal", type2: "None", },
	"Riolu": { type1: "Fighting", type2: "None", },
	"Lucario": { type1: "Fighting", type2: "Steel", },
	"Hippopotas": { type1: "Ground", type2: "None", },
	"Hippowdon": { type1: "Ground", type2: "None", },
	"Skorupi": { type1: "Poison", type2: "Bug", },
	"Drapion": { type1: "Poison", type2: "Dark", },
	"Croagunk": { type1: "Poison", type2: "Fighting", },
	"Toxicroak": { type1: "Poison", type2: "Fighting", },
	"Carnivine": { type1: "Grass", type2: "None", },
	"Finneon": { type1: "Water", type2: "None", },
	"Lumineon": { type1: "Water", type2: "None", },
	"Mantyke": { type1: "Water", type2: "Flying", },
	"Snover": { type1: "Grass", type2: "Ice", },
	"Abomasnow": { type1: "Grass", type2: "Ice", },
	"Weavile": { type1: "Dark", type2: "Ice", },
	"Magnezone": { type1: "Electric", type2: "Steel", },
	"Lickilicky": { type1: "Normal", type2: "None", },
	"Rhyperior": { type1: "Ground", type2: "Rock", },
	"Tangrowth": { type1: "Grass", type2: "None", },
	"Electivire": { type1: "Electric", type2: "None", },
	"Magmortar": { type1: "Fire", type2: "None", },
	"Togekiss": { type1: "Fairy", type2: "Flying", },
	"Yanmega": { type1: "Bug", type2: "Flying", },
	"Leafeon": { type1: "Grass", type2: "None", },
	"Glaceon": { type1: "Ice", type2: "None", },
	"Gliscor": { type1: "Ground", type2: "Flying", },
	"Mamoswine": { type1: "Ice", type2: "Ground", },
	"Porygon-Z": { type1: "Normal", type2: "None", },
	"Gallade": { type1: "Psychic", type2: "Fighting", },
	"Probopass": { type1: "Rock", type2: "Steel", },
	"Dusknoir": { type1: "Ghost", type2: "None", },
	"Froslass": { type1: "Ice", type2: "Ghost", },
	"Rotom": { type1: "Electric", type2: "Ghost", },
	"Uxie": { type1: "Psychic", type2: "None", },
	"Mesprit": { type1: "Psychic", type2: "None", },
	"Azelf": { type1: "Psychic", type2: "None", },
	"Dialga": { type1: "Steel", type2: "Dragon", },
	"Palkia": { type1: "Water", type2: "Dragon", },
	"Heatran": { type1: "Fire", type2: "Steel", },
	"Regigigas": { type1: "Normal", type2: "None", },
	"Giratina": { type1: "Ghost", type2: "Dragon", },
	"Cresselia": { type1: "Psychic", type2: "None", },
	"Phione": { type1: "Water", type2: "None", },
	"Manaphy": { type1: "Water", type2: "None", },
	"Darkrai": { type1: "Dark", type2: "None", },
	"Shaymin": { type1: "Grass", type2: "None", },
	"Arceus": { type1: "Normal", type2: "None", },
	"Victini": { type1: "Psychic", type2: "Fire", },
	"Snivy": { type1: "Grass", type2: "None", },
	"Servine": { type1: "Grass", type2: "None", },
	"Serperior": { type1: "Grass", type2: "None", },
	"Tepig": { type1: "Fire", type2: "None", },
	"Pignite": { type1: "Fire", type2: "Fighting", },
	"Emboar": { type1: "Fire", type2: "Fighting", },
	"Oshawott": { type1: "Water", type2: "None", },
	"Dewott": { type1: "Water", type2: "None", },
	"Samurott": { type1: "Water", type2: "None", },
	"Patrat": { type1: "Normal", type2: "None", },
	"Watchog": { type1: "Normal", type2: "None", },
	"Lillipup": { type1: "Normal", type2: "None", },
	"Herdier": { type1: "Normal", type2: "None", },
	"Stoutland": { type1: "Normal", type2: "None", },
	"Purrloin": { type1: "Dark", type2: "None", },
	"Liepard": { type1: "Dark", type2: "None", },
	"Pansage": { type1: "Grass", type2: "None", },
	"Simisage": { type1: "Grass", type2: "None", },
	"Pansear": { type1: "Fire", type2: "None", },
	"Simisear": { type1: "Fire", type2: "None", },
	"Panpour": { type1: "Water", type2: "None", },
	"Simipour": { type1: "Water", type2: "None", },
	"Munna": { type1: "Psychic", type2: "None", },
	"Musharna": { type1: "Psychic", type2: "None", },
	"Pidove": { type1: "Normal", type2: "Flying", },
	"Tranquill": { type1: "Normal", type2: "Flying", },
	"Unfezant": { type1: "Normal", type2: "Flying", },
	"Blitzle": { type1: "Electric", type2: "None", },
	"Zebstrika": { type1: "Electric", type2: "None", },
	"Roggenrola": { type1: "Rock", type2: "None", },
	"Boldore": { type1: "Rock", type2: "None", },
	"Gigalith": { type1: "Rock", type2: "None", },
	"Woobat": { type1: "Psychic", type2: "Flying", },
	"Swoobat": { type1: "Psychic", type2: "Flying", },
	"Drilbur": { type1: "Ground", type2: "None", },
	"Excadrill": { type1: "Ground", type2: "Steel", },
	"Audino": { type1: "Normal", type2: "None", },
	"Timburr": { type1: "Fighting", type2: "None", },
	"Gurdurr": { type1: "Fighting", type2: "None", },
	"Conkeldurr": { type1: "Fighting", type2: "None", },
	"Tympole": { type1: "Water", type2: "None", },
	"Palpitoad": { type1: "Water", type2: "Ground", },
	"Seismitoad": { type1: "Water", type2: "Ground", },
	"Throh": { type1: "Fighting", type2: "None", },
	"Sawk": { type1: "Fighting", type2: "None", },
	"Sewaddle": { type1: "Bug", type2: "Grass", },
	"Swadloon": { type1: "Bug", type2: "Grass", },
	"Leavanny": { type1: "Bug", type2: "Grass", },
	"Venipede": { type1: "Bug", type2: "Poison", },
	"Whirlipede": { type1: "Bug", type2: "Poison", },
	"Scolipede": { type1: "Bug", type2: "Poison", },
	"Cottonee": { type1: "Grass", type2: "Fairy", },
	"Whimsicott": { type1: "Grass", type2: "Fairy", },
	"Petilil": { type1: "Grass", type2: "None", },
	"Lilligant": { type1: "Grass", type2: "None", },
	"Basculin": { type1: "Water", type2: "None", },
	"Sandile": { type1: "Ground", type2: "Dark", },
	"Krokorok": { type1: "Ground", type2: "Dark", },
	"Krookodile": { type1: "Ground", type2: "Dark", },
	"Darumaka": { type1: "Fire", type2: "None", },
	"Darmanitan": { type1: "Fire", type2: "None", },
	"Maractus": { type1: "Grass", type2: "None", },
	"Dwebble": { type1: "Bug", type2: "Rock", },
	"Crustle": { type1: "Bug", type2: "Rock", },
	"Scraggy": { type1: "Dark", type2: "Fighting", },
	"Scrafty": { type1: "Dark", type2: "Fighting", },
	"Sigilyph": { type1: "Psychic", type2: "Flying", },
	"Yamask": { type1: "Ghost", type2: "None", },
	"Cofagrigus": { type1: "Ghost", type2: "None", },
	"Tirtouga": { type1: "Water", type2: "Rock", },
	"Carracosta": { type1: "Water", type2: "Rock", },
	"Archen": { type1: "Rock", type2: "Flying", },
	"Archeops": { type1: "Rock", type2: "Flying", },
	"Trubbish": { type1: "Poison", type2: "None", },
	"Garbodor": { type1: "Poison", type2: "None", },
	"Zorua": { type1: "Dark", type2: "None", },
	"Zoroark": { type1: "Dark", type2: "None", },
	"Minccino": { type1: "Normal", type2: "None", },
	"Cinccino": { type1: "Normal", type2: "None", },
	"Gothita": { type1: "Psychic", type2: "None", },
	"Gothorita": { type1: "Psychic", type2: "None", },
	"Gothitelle": { type1: "Psychic", type2: "None", },
	"Solosis": { type1: "Psychic", type2: "None", },
	"Duosion": { type1: "Psychic", type2: "None", },
	"Reuniclus": { type1: "Psychic", type2: "None", },
	"Ducklett": { type1: "Water", type2: "Flying", },
	"Swanna": { type1: "Water", type2: "Flying", },
	"Vanillite": { type1: "Ice", type2: "None", },
	"Vanillish": { type1: "Ice", type2: "None", },
	"Vanilluxe": { type1: "Ice", type2: "None", },
	"Deerling": { type1: "Normal", type2: "Grass", },
	"Sawsbuck": { type1: "Normal", type2: "Grass", },
	"Emolga": { type1: "Electric", type2: "Flying", },
	"Karrablast": { type1: "Bug", type2: "None", },
	"Escavalier": { type1: "Bug", type2: "Steel", },
	"Foongus": { type1: "Grass", type2: "Poison", },
	"Amoonguss": { type1: "Grass", type2: "Poison", },
	"Frillish": { type1: "Water", type2: "Ghost", },
	"Jellicent": { type1: "Water", type2: "Ghost", },
	"Alomomola": { type1: "Water", type2: "None", },
	"Joltik": { type1: "Bug", type2: "Electric", },
	"Galvantula": { type1: "Bug", type2: "Electric", },
	"Ferroseed": { type1: "Grass", type2: "Steel", },
	"Ferrothorn": { type1: "Grass", type2: "Steel", },
	"Klink": { type1: "Steel", type2: "None", },
	"Klang": { type1: "Steel", type2: "None", },
	"Klinklang": { type1: "Steel", type2: "None", },
	"Tynamo": { type1: "Electric", type2: "None", },
	"Eelektrik": { type1: "Electric", type2: "None", },
	"Eelektross": { type1: "Electric", type2: "None", },
	"Elgyem": { type1: "Psychic", type2: "None", },
	"Beheeyem": { type1: "Psychic", type2: "None", },
	"Litwick": { type1: "Ghost", type2: "Fire", },
	"Lampent": { type1: "Ghost", type2: "Fire", },
	"Chandelure": { type1: "Ghost", type2: "Fire", },
	"Axew": { type1: "Dragon", type2: "None", },
	"Fraxure": { type1: "Dragon", type2: "None", },
	"Haxorus": { type1: "Dragon", type2: "None", },
	"Cubchoo": { type1: "Ice", type2: "None", },
	"Beartic": { type1: "Ice", type2: "None", },
	"Cryogonal": { type1: "Ice", type2: "None", },
	"Shelmet": { type1: "Bug", type2: "None", },
	"Accelgor": { type1: "Bug", type2: "None", },
	"Stunfisk": { type1: "Ground", type2: "Electric", },
	"Mienfoo": { type1: "Fighting", type2: "None", },
	"Mienshao": { type1: "Fighting", type2: "None", },
	"Druddigon": { type1: "Dragon", type2: "None", },
	"Golett": { type1: "Ground", type2: "Ghost", },
	"Golurk": { type1: "Ground", type2: "Ghost", },
	"Pawniard": { type1: "Dark", type2: "Steel", },
	"Bisharp": { type1: "Dark", type2: "Steel", },
	"Bouffalant": { type1: "Normal", type2: "None", },
	"Rufflet": { type1: "Normal", type2: "Flying", },
	"Braviary": { type1: "Normal", type2: "Flying", },
	"Vullaby": { type1: "Dark", type2: "Flying", },
	"Mandibuzz": { type1: "Dark", type2: "Flying", },
	"Heatmor": { type1: "Fire", type2: "None", },
	"Durant": { type1: "Bug", type2: "Steel", },
	"Deino": { type1: "Dark", type2: "Dragon", },
	"Zweilous": { type1: "Dark", type2: "Dragon", },
	"Hydreigon": { type1: "Dark", type2: "Dragon", },
	"Larvesta": { type1: "Bug", type2: "Fire", },
	"Volcarona": { type1: "Bug", type2: "Fire", },
	"Cobalion": { type1: "Steel", type2: "Fighting", },
	"Terrakion": { type1: "Rock", type2: "Fighting", },
	"Virizion": { type1: "Grass", type2: "Fighting", },
	"Tornadus": { type1: "Flying", type2: "None", },
	"Thundurus": { type1: "Electric", type2: "Flying", },
	"Reshiram": { type1: "Dragon", type2: "Fire", },
	"Zekrom": { type1: "Dragon", type2: "Electric", },
	"Landorus": { type1: "Ground", type2: "Flying", },
	"Kyurem": { type1: "Dragon", type2: "Ice", },
	"Keldeo": { type1: "Water", type2: "Fighting", },
	"Meloetta": { type1: "Normal", type2: "Psychic", },
	"Genesect": { type1: "Bug", type2: "Steel", },
	"Chespin": { type1: "Grass", type2: "None", },
	"Quilladin": { type1: "Grass", type2: "None", },
	"Chesnaught": { type1: "Grass", type2: "Fighting", },
	"Fennekin": { type1: "Fire", type2: "None", },
	"Braixen": { type1: "Fire", type2: "None", },
	"Delphox": { type1: "Fire", type2: "Psychic", },
	"Froakie": { type1: "Water", type2: "None", },
	"Frogadier": { type1: "Water", type2: "None", },
	"Greninja": { type1: "Water", type2: "Dark", },
	"Bunnelby": { type1: "Normal", type2: "None", },
	"Diggersby": { type1: "Normal", type2: "Ground", },
	"Fletchling": { type1: "Normal", type2: "Flying", },
	"Fletchinder": { type1: "Fire", type2: "Flying", },
	"Talonflame": { type1: "Fire", type2: "Flying", },
	"Scatterbug": { type1: "Bug", type2: "None", },
	"Spewpa": { type1: "Bug", type2: "None", },
	"Vivillon": { type1: "Bug", type2: "Flying", },
	"Litleo": { type1: "Fire", type2: "Normal", },
	"Pyroar": { type1: "Fire", type2: "Normal", },
	"Flabébé": { type1: "Fairy", type2: "None", },
	"Floette": { type1: "Fairy", type2: "None", },
	"Florges": { type1: "Fairy", type2: "None", },
	"Skiddo": { type1: "Grass", type2: "None", },
	"Gogoat": { type1: "Grass", type2: "None", },
	"Pancham": { type1: "Fighting", type2: "None", },
	"Pangoro": { type1: "Fighting", type2: "Dark", },
	"Furfrou": { type1: "Normal", type2: "None", },
	"Espurr": { type1: "Psychic", type2: "None", },
	"Meowstic": { type1: "Psychic", type2: "None", },
	"Honedge": { type1: "Steel", type2: "Ghost", },
	"Doublade": { type1: "Steel", type2: "Ghost", },
	"Aegislash": { type1: "Steel", type2: "Ghost", },
	"Spritzee": { type1: "Fairy", type2: "None", },
	"Aromatisse": { type1: "Fairy", type2: "None", },
	"Swirlix": { type1: "Fairy", type2: "None", },
	"Slurpuff": { type1: "Fairy", type2: "None", },
	"Inkay": { type1: "Dark", type2: "Psychic", },
	"Malamar": { type1: "Dark", type2: "Psychic", },
	"Binacle": { type1: "Rock", type2: "Water", },
	"Barbaracle": { type1: "Rock", type2: "Water", },
	"Skrelp": { type1: "Poison", type2: "Water", },
	"Dragalge": { type1: "Poison", type2: "Dragon", },
	"Clauncher": { type1: "Water", type2: "None", },
	"Clawitzer": { type1: "Water", type2: "None", },
	"Helioptile": { type1: "Electric", type2: "Normal", },
	"Heliolisk": { type1: "Electric", type2: "Normal", },
	"Tyrunt": { type1: "Rock", type2: "Dragon", },
	"Tyrantrum": { type1: "Rock", type2: "Dragon", },
	"Amaura": { type1: "Rock", type2: "Ice", },
	"Aurorus": { type1: "Rock", type2: "Ice", },
	"Sylveon": { type1: "Fairy", type2: "None", },
	"Hawlucha": { type1: "Fighting", type2: "Flying", },
	"Dedenne": { type1: "Electric", type2: "Fairy", },
	"Carbink": { type1: "Rock", type2: "Fairy", },
	"Goomy": { type1: "Dragon", type2: "None", },
	"Sliggoo": { type1: "Dragon", type2: "None", },
	"Goodra": { type1: "Dragon", type2: "None", },
	"Klefki": { type1: "Steel", type2: "Fairy", },
	"Phantump": { type1: "Ghost", type2: "Grass", },
	"Trevenant": { type1: "Ghost", type2: "Grass", },
	"Pumpkaboo": { type1: "Ghost", type2: "Grass", },
	"Gourgeist": { type1: "Ghost", type2: "Grass", },
	"Bergmite": { type1: "Ice", type2: "None", },
	"Avalugg": { type1: "Ice", type2: "None", },
	"Noibat": { type1: "Flying", type2: "Dragon", },
	"Noivern": { type1: "Flying", type2: "Dragon", },
	"Xerneas": { type1: "Fairy", type2: "None", },
	"Yveltal": { type1: "Dark", type2: "Flying", },
	"Zygarde": { type1: "Dragon", type2: "Ground", },
	"Diancie": { type1: "Rock", type2: "Fairy", },
	"Hoopa": { type1: "Psychic", type2: "Ghost", },
	"Volcanion": { type1: "Fire", type2: "Water", },
	"Rowlet": { type1: "Grass", type2: "Flying", },
	"Dartrix": { type1: "Grass", type2: "Flying", },
	"Decidueye": { type1: "Grass", type2: "Ghost", },
	"Litten": { type1: "Fire", type2: "None", },
	"Torracat": { type1: "Fire", type2: "None", },
	"Incineroar": { type1: "Fire", type2: "Dark", },
	"Popplio": { type1: "Water", type2: "None", },
	"Brionne": { type1: "Water", type2: "None", },
	"Primarina": { type1: "Water", type2: "Fairy", },
	"Pikipek": { type1: "Normal", type2: "Flying", },
	"Trumbeak": { type1: "Normal", type2: "Flying", },
	"Toucannon": { type1: "Normal", type2: "Flying", },
	"Yungoos": { type1: "Normal", type2: "None", },
	"Gumshoos": { type1: "Normal", type2: "None", },
	"Grubbin": { type1: "Bug", type2: "None", },
	"Charjabug": { type1: "Bug", type2: "Electric", },
	"Vikavolt": { type1: "Bug", type2: "Electric", },
	"Crabrawler": { type1: "Fighting", type2: "None", },
	"Crabominable": { type1: "Fighting", type2: "Ice", },
	"Oricorio": { type1: "Electric", type2: "Flying", },
	"Cutiefly": { type1: "Bug", type2: "Fairy", },
	"Ribombee": { type1: "Bug", type2: "Fairy", },
	"Rockruff": { type1: "Rock", type2: "None", },
	"Lycanroc": { type1: "Rock", type2: "None", },
	"Wishiwashi": { type1: "Water", type2: "None", },
	"Mareanie": { type1: "Poison", type2: "Water", },
	"Toxapex": { type1: "Poison", type2: "Water", },
	"Mudbray": { type1: "Ground", type2: "None", },
	"Mudsdale": { type1: "Ground", type2: "None", },
	"Dewpider": { type1: "Water", type2: "Bug", },
	"Araquanid": { type1: "Water", type2: "Bug", },
	"Fomantis": { type1: "Grass", type2: "Bug", },
	"Lurantis": { type1: "Grass", type2: "None", },
	"Morelull": { type1: "Grass", type2: "Fairy", },
	"Shiinotic": { type1: "Grass", type2: "Fairy", },
	"Salandit": { type1: "Poison", type2: "Fire", },
	"Salazzle": { type1: "Poison", type2: "Fire", },
	"Stufful": { type1: "Normal", type2: "Fighting", },
	"Bewear": { type1: "Normal", type2: "Fighting", },
	"Bounsweet": { type1: "Grass", type2: "None", },
	"Steenee": { type1: "Grass", type2: "None", },
	"Tsareena": { type1: "Grass", type2: "None", },
	"Comfey": { type1: "Fairy", type2: "None", },
	"Oranguru": { type1: "Normal", type2: "Psychic", },
	"Passimian": { type1: "Fighting", type2: "None", },
	"Wimpod": { type1: "Bug", type2: "Water", },
	"Golisopod": { type1: "Bug", type2: "Water", },
	"Sandygast": { type1: "Ghost", type2: "Ground", },
	"Palossand": { type1: "Ghost", type2: "Ground", },
	"Pyukumuku": { type1: "Water", type2: "None", },
	"Type: Null": { type1: "Normal", type2: "None", },
	"Silvally": { type1: "Normal", type2: "None", },
	"Minior": { type1: "Rock", type2: "Flying", },
	"Komala": { type1: "Normal", type2: "None", },
	"Turtonator": { type1: "Fire", type2: "Dragon", },
	"Togedemaru": { type1: "Electric", type2: "Steel", },
	"Mimikyu": { type1: "Ghost", type2: "Fairy", },
	"Bruxish": { type1: "Water", type2: "Psychic", },
	"Drampa": { type1: "Normal", type2: "Dragon", },
	"Dhelmise": { type1: "Ghost", type2: "Grass", },
	"Jangmo-o": { type1: "Dragon", type2: "None", },
	"Hakamo-o": { type1: "Dragon", type2: "Fighting", },
	"Kommo-o": { type1: "Dragon", type2: "Fighting", },
	"Tapu Koko": { type1: "Electric", type2: "Fairy", },
	"Tapu Lele": { type1: "Psychic", type2: "Fairy", },
	"Tapu Bulu": { type1: "Grass", type2: "Fairy", },
	"Tapu Fini": { type1: "Water", type2: "Fairy", },
	"Cosmog": { type1: "Psychic", type2: "None", },
	"Cosmoem": { type1: "Psychic", type2: "None", },
	"Solgaleo": { type1: "Psychic", type2: "Steel", },
	"Lunala": { type1: "Psychic", type2: "Ghost", },
	"Nihilego": { type1: "Rock", type2: "Poison", },
	"Buzzwole": { type1: "Bug", type2: "Fighting", },
	"Pheromosa": { type1: "Bug", type2: "Fighting", },
	"Xurkitree": { type1: "Electric", type2: "None", },
	"Celesteela": { type1: "Steel", type2: "Flying", },
	"Kartana": { type1: "Grass", type2: "Steel", },
	"Guzzlord": { type1: "Dark", type2: "Dragon", },
	"Necrozma": { type1: "Psychic", type2: "None", },
	"Magearna": { type1: "Steel", type2: "Fairy", },
	"Marshadow": { type1: "Fighting", type2: "Ghost", },
};
