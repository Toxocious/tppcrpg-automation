// ==UserScript==
// @name         TPPC v8: SS Anne
// @description  Auto-battles the SS Anne in an attempt to beat out the current margin by +1000.
// @include      https://*tppcrpg.net*
// @grant        GM_deleteValue
// @grant        GM_getValue
// @grant        GM_setValue
// @namespace    https://gitlab.com/toxocious
// @author       Toxocious
// @version      8.0
// ==/UserScript==

/* ====================================================================
                               TO-DO LIST
   ------------------------------------------------------------------
   -> Check for current status ailment on your Chansey/Blissey.
   --> If you have a status ailment, swap out, and swap back in.

   -> RESUME BATTLE
   --> Due to the battle id generated for resuming battles,
       it fucks up the script since we look for a certain url.
==================================================================== */

// Variables :: Don't touch these.
var firstPlace = 0;
var randClick = 0;
var clicked = 0;
var count = 0;
var set = 0;
var inputWidth = Math.floor(Math.random() * 50 + 20);
var inputHeight = Math.floor(Math.random() * 6 + 10);

// When the DOM loads.
$(function() {
  // Detect if the user is on the SS Anne page.
  if ( window.location.href.indexOf("ss_anne") > -1 )
  {
    // Reset GM vals.
    GM_setValue("currentStreak", 0);
    GM_setValue("movesUsed", 0);

    // Determine what the current margin is to win a G.Gene.
    if ( $('p').text().indexOf('No one has completed the S.S. Anne Endurance Battle yet') != -1 )
      firstPlace = '0';
    else
      firstPlace = $('#inner > table:nth-child(9) > tbody > tr:nth-child(1) > td:nth-child(4)').text();

    // Prase integers to get and set goalMargin variable.
    battleMargin = $('#inner > p:nth-child(6)').text().substr(72, 6);
    goalMargin = parseFloat(battleMargin.replace(/,/g, '')) + parseFloat(firstPlace.replace(/,/g, '')) + 1000;
    GM_setValue('goalMargin', goalMargin);

    // Make sure the user's active Pokemon is a Chansey or Blissey.
    if ( $('#right ul li:nth-child(3)').text().indexOf('Chansey') != -1 || $('#right ul li:nth-child(3)').text().indexOf("Blissey") != -1 )
    {
      // If the user's active Pokemon IS a Chansey/Blissey, begin the battle.
      //window.location = 'http://www.tppcrpg.net/battle.php?Battle=SSAnne';
    }
    else
    {
      alert("Swap your active Pokemon into a Chansey or Blissey to continue.");
    }
  }

  // Detect if the user has started battling the SS Anne.
  // http://www.tppcrpg.net/battle.php?Battle=SSAnne
  if ( window.location.href.indexOf('SSAnne') > -1 )
  {
    interval = setInterval(function()
    {
      if ( GM_getValue("currentStreak") < GM_getValue('goalMargin') )
      {
        if (document.body.innerHTML.search("lease choose your next") != -1)
        {
          console.log("Your Chansey/Blissey has fainted while your current streak was less than the goal streak.");
          window.location = 'http://www.tppcrpg.net/ss_anne.php';
        }
      }

      if (!GM_getValue("currentStreak"))
        GM_setValue("currentStreak", 0);

      if (!GM_getValue("movesUsed"))
        GM_setValue("movesUsed", 0);

      if (!GM_getValue("shortpause"))
        GM_setValue("shortpause", 0);

      if (!GM_getValue("longpause"))
        GM_setValue("longpause", 0);

      if(set === 0)
      {
        $(".lvlitem").append("<br><a href='#' style='color:#FFFFFF !important;text-decoration:none;'>Pause Count:</a> " + GM_getValue("shortpause") + " <br/>Long Pause: " + GM_getValue("longpause"));
      }
      set = 1;

      $(".lvlitem a").click(function()
      {
        if ( clicked === 0 )
        {
          if ( confirm("Reset counter?") )
          {
            clicked = 1;
            clearInterval(interval);
            GM_deleteValue("longpause");
            GM_deleteValue("shortpause");
          }
        }
      });

      // Get battle button position and top left, bottom right coords.
      x = $('.submit').position();
      cordX = x.left;
      cordY = x.top;

      if (document.body.innerHTML.lastIndexOf(/Loading.../) == -1) {
      inputs = document.getElementsByTagName("input");
      randInt = Math.floor(Math.random() * 720 + 1);

      if (randInt == 1) {
        randClick = Math.floor(Math.random() * 25000) + 5000;
        sleep(randClick);
        var shortpause = GM_getValue("shortpause") + 1;
        GM_setValue("shortpause", shortpause);
      }

      // Attack the foe.
      for ( i = 0; i < inputs.length; i++ )
      {
        if ( inputs[i].value.search(/(Attack)/) != -1 )
        {
          // NOTE :: ALL OF THE ROSTER'S POKEMON'S MOVES HAVE TO BE SET TO THESE.
          // Verify that the user has the correct moves.
          // Move #1: Ice Beam      (149)
          // Move #2: Ancient Power (264)
          // Move #3: Double Edge   (168)
          // Move #4: Giga Drain    (114)
          if
          (
            $('#MyMove option:nth-child(1)').val() !== '149' &&
            $('#MyMove option:nth-child(2)').val() !== '264' &&
            $('#MyMove option:nth-child(3)').val() !== '168' &&
            $('#MyMove option:nth-child(4)').val() !== '114'
          )
          {
            clearInterval(interval);
            alert('Please change your moves to: Ice Beam, Ancient Power, Double Edge, and Giga Drain.');
            return true;
          }

          count = 0;

          var foe = $('#Trainer2_Active > div.headerBar').text().split("Captain's ", 2).join("");
          var curHP = $('#Trainer1_Active .hpBar').attr('style').split('width: ', 2).join("").slice(0, -2);

          // If your current streak is less than the goal margin.
          if ( GM_getValue('currentStreak') < GM_getValue('goalMargin') )
          {
            // If you have less than 75% HP, prioritize Giga Drain.
            if ( curHP < 75 )
            {
              console.log("You have less than 75% HP.");
              Move = '114';
            }
            else
            {
              console.log("You have more than 75% HP.");

              // Faint at least four Pokemon with Ancient Power.
              if ( GM_getValue('currentStreak') < 4 )
              {
                Move = '264';
              }
              else
              {
                Move = '149';
              }
            }
          }
          else
          {
            Move = '168';
          }

          // Increase move counter.
          var movesInc = GM_getValue('movesUsed') + 1;
          GM_setValue('movesUsed', movesInc);

          // Perform the attack input.
          unsafeWindow.getBattleStatus({
            "MyMove": Move,
            "pageID": Math.floor(Math.random() * inputHeight + cordX + inputWidth) + "." + Math.floor(Math.random() * 18 + cordY + 3)
          });
        }

        if (inputs[i].value.search(/(Continue)/) != -1)
        {
          var streakInc = GM_getValue('currentStreak') + 1;
          GM_setValue('currentStreak', streakInc);

          unsafeWindow.getBattleStatus({
            "MyMove": "WaitFaint",
            "pageID": Math.floor(Math.random() * inputHeight + cordX + inputWidth) + "." + Math.floor(Math.random() * 15 + cordY + 2)
          });
        }
      }
      }
    }, Math.random() * 400 + 500);
  }

  // If a captcha shows on screen, alert the user.
  if (document.body.innerHTML.search(/Please Enter The Combination You See Above/) != -1)
  {
    alert("Captcha!");
  }

  // Once the user has submitted the correct captcha, go to the training challenge page.
  if (document.body.innerHTML.search(/You can now resume battling/) != -1)
  {
    console.log('Redirecting the script to the SS Anne page.');
    window.location = "http://www.tppcrpg.net/ss_anne.php";
  }

  // Once on the training challenge page, begin battling.
  if (window.location == "http://www.tppcrpg.net/ss_anne.php")
  {
    console.log('Beginning the battle against the SS Anne NPC.');
    window.location = "http://www.tppcrpg.net/battle.php?Battle=SSAnne";
  }
});
