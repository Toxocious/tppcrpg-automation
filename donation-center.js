// ==UserScript==
// @name         TPPC v8: Donation Center
// @description  Automatically donate Pokemon to the Donation Center.
// @match        https://www.tppcrpg.net/donation_center.php*
// @require      https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js
// @namespace    https://gitlab.com/toxocious
// @author       Toxocious
// @version      8.0
// ==/UserScript==

$(function()
{
  setInterval(function()
  {
    if ( $('#inner p').text().indexOf('You do not have any') > -1 )
    {
      interval = setInterval(function()
      {
        clearInterval(interval);
        window.location = 'http://www.tppcrpg.net/donation_center.php';
      }, Math.random() * 400 + 250);
    }
    else
    {
      $('select[name="donatePoke"] option:eq(1)').attr('selected', 'selected');

      if (
        $('select[name="donatePoke"] option:eq(1)').text().indexOf('Level: 5') > -1 ||
        $('select[name="donatePoke"] option:eq(1)').text().indexOf('(?)') > -1 ||
        $('select[name="donatePoke"] option:eq(1)').text().indexOf('Golden') > -1 ||
        $('select[name="donatePoke"] option:eq(1)').text().indexOf('Shiny') > -1 ||
        $('select[name="donatePoke"] option:eq(1)').text().indexOf('Dark') > -1
      )
      {
        console.log('ungendered found; refreshing');
        window.location = 'http://www.tppcrpg.net/donation_center.php';
      }
      else
      {
        $('input[value="Donate Pokémon"]').click();
      }
    }
  }, Math.random() * 400 + 250);
});
